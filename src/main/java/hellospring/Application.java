package hellospring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {
	
	@RequestMapping("/")
    @ResponseBody
    String home() {
        return "Hello Spring!";
    }
	
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
